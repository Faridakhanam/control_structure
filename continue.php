<?php
//example 1
$num = 0;

while ($num < 20)
{
    $num++;
    if ($num == 15) {
        continue;
    }
    else {
        echo "Continue at 15 (".$num.").<br />";
    }

}
//example 2
echo "<br>";
$num = 0;

while ($num < 15)
{
    $num++;
    if ($num == 12) {
        continue;
    }
    else {
        echo "Continue at 12 (".$num.").<br />";
    }

}
?>